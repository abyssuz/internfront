import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { AdminContext } from '../context';

export const useManageList = (url) => {
  const [list, setList] = useState([]);
  const { setIsAuth } = useContext(AdminContext);
  const router = useHistory();

  useEffect(() => {
    fetchList();
  }, []);

  const fetchList = async () => {
    const response = await fetch(url, {
      headers: { authorization: `Bearer ${localStorage.getItem('token')}` },
    });
    if (response.status === 401) {
      router.push('/admin');
      setIsAuth(false);
    } else {
      const list = await response.json();
      setList(list);
    }
  };

  const add = async (note) => {
    const response = await fetch(url, {
      method: 'POST',
      body: JSON.stringify(note),
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
    if (response.status === 401) {
      router.push('/admin');
      setIsAuth(false);
    } else {
      const newNote = await response.json();
      setList([...list, newNote]);
    }
  };

  const edit = async (note) => {
    console.log(note)
    const response = await fetch(`${url}/${note.id}`, {
      method: 'PUT',
      body: JSON.stringify(note),
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
    if (response.status === 401) {
      router.push('/admin');
      setIsAuth(false);
    } else {
      const editedNote = await response.json();
      setList(list.map((el) => (el.id === editedNote.id ? editedNote : el)));
    }
  };

  const remove = async (id) => {
    const response = await fetch(`${url}/${id}`, {
      method: 'DELETE',
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    });
    if (response.status === 401) {
      router.push('/admin');
      setIsAuth(false);
    } else setList(list.filter((el) => el.id !== id));
  };

  return [list, remove, add, edit];
};
