import React, { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { SuccessfulOrderContext } from './context';
import AppRouter from './AppRouter';
import './main.css';

const App = () => {
  const [orderDetails, setOrderDetails] = useState({});

  return (
    <SuccessfulOrderContext.Provider value={{ orderDetails, setOrderDetails }}>
      <BrowserRouter>
        <AppRouter></AppRouter>
      </BrowserRouter>
    </SuccessfulOrderContext.Provider>
  );
};

export default App;
