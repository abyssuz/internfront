import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import ReactStars from 'react-rating-stars-component';
import SubmitButton from '../Components/commonUI/SubmitButton';
import { useQuery } from '../hooks/useQuery';
import './RatePage.css';

const RatePage = () => {
  const [isAllowed, setIsAllowed] = useState(false);
  const [rate, setRate] = useState(0);
  const [master, setMaster] = useState({});
  const router = useHistory();
  const query = useQuery();

  useEffect(async () => {
    const response = await fetch(`${process.env.API_URL}/orders/${query.get('link')}`);
    if (response.status === 200) {
      const master = await response.json();
      setIsAllowed(true);
      setMaster(master);
    }
  }, []);

  const ratingChanged = (rating) => {
    setRate(rating);
  };

  const onClickBtn = (e) => {
    e.preventDefault();
    fetch(`${process.env.API_URL}/masters/rate/${master.id}?link=${query.get('link')}`, {
      method: 'PUT',
      body: JSON.stringify({ rate }),
      headers: { 'Content-Type': 'application/json' },
    });
    router.push('/');
  };

  return isAllowed ? (
    <div className="rate-page">
      <h1>Rate for {master.name}</h1>
      <ReactStars
        count={5}
        onChange={ratingChanged}
        size={48}
        activeColor="#ffd700"
      />
      <SubmitButton onClick={onClickBtn}></SubmitButton>
    </div>
  ) : (
    <div className="rate-page">Something wrong :(</div>
  );
};

export default RatePage;
