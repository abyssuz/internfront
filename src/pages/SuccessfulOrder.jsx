import React, { useContext, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import SubmitButton from '../Components/commonUI/SubmitButton';
import { SuccessfulOrderContext } from '../context';
import './SuccessfulOrder.css';

const SuccessfulOrder = () => {
  const { orderDetails, setOrderDetails } = useContext(SuccessfulOrderContext);
  const router = useHistory();
  const { id } = useParams();

  useEffect(() => {
    console.log(JSON.stringify(orderDetails));
    if (isNaN(id) || JSON.stringify(orderDetails) === '{}') router.push('/');
  }, []);

  return (
    <div className="successful-order">
      <h2>Order details</h2>
      {Object.keys(orderDetails).map((el, index) =>
        el.match(/id|link/gi) === null ? (
          <p key={index}>{`${el.toUpperCase()}: ${orderDetails[el]}`}</p>
        ) : (
          ''
        )
      )}
      <h3>Order info has been sent to your email</h3>
      <SubmitButton
        value="Return back"
        onClick={() => {
          setOrderDetails({});
          router.goBack();
        }}
      />
    </div>
  );
};

export default SuccessfulOrder;
