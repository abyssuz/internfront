import React, { useContext, useMemo, useState } from 'react';
import { useHistory } from 'react-router';
import './OrderForm.css';
import TextInput from '../Components/commonUI/TextInput';
import ClocksizeRadio from '../Components/OrderForm/ui/ClocksizeSelect';
import CitySelect from '../Components/OrderForm/ui/CitySelect';
import DatePicker from '../Components/OrderForm/ui/DatePicker';
import TimeSelect from '../Components/OrderForm/ui/TimeSelect';
import MastersSelect from '../Components/OrderForm/ui/MastersSelect';
import SubmitButton from '../Components/commonUI/SubmitButton';
import { SuccessfulOrderContext } from '../context';

const OrderForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [clockSize, setClockSize] = useState('');
  const [cityId, setCityId] = useState('');
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');
  const [master, setMaster] = useState({ id: '', name: '' });
  const [isNameValid, setNameValid] = useState(true);
  const [isEmailValid, setEmailValid] = useState(true);
  const [isFormFilled, setFormFilled] = useState(true);
  const { setOrderDetails } = useContext(SuccessfulOrderContext);
  const router = useHistory();

  const showMasters = useMemo(() => {
    if (clockSize === '' || cityId === '' || date === '' || time === '')
      return false;
    else return true;
  }, [clockSize, cityId, date, time]);

  const isFormValid = useMemo(() => {
    if (
      !isNameValid ||
      !isEmailValid ||
      clockSize === '' ||
      cityId === '' ||
      date === '' ||
      time === '' ||
      master.name === ''
    )
      return false;
    else return true;
  }, [isNameValid, isEmailValid, clockSize, cityId, date, time, master]);

  const onOrderBtnClick = async (e) => {
    e.preventDefault();

    if (isFormValid) {
      const newOrder = {
        name,
        email,
        clockSize,
        cityId,
        date,
        time,
        masterId: master.id,
      };

      const response = await fetch(process.env.API_URL + 'orders', {
        method: 'POST',
        body: JSON.stringify(newOrder),
        headers: { 'Content-Type': 'application/json' },
      });
      if (response.status === 201) {
        const createdOrderDetails = await response.json();

        setOrderDetails({
          name,
          email,
          ...createdOrderDetails,
          master: master.name,
        });

        router.push(`/successfulorder/${createdOrderDetails.id}`);
      } else {
        router.push('/error')
      }
    }
  };

  const onInputFocus = (e) => {
    switch (e.target.dataset.field) {
      case 'name':
        setNameValid(true);
        break;
      case 'email':
        setEmailValid(true);
        break;
    }
  };

  const onInputBlur = (e) => {
    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    switch (e.target.dataset.field) {
      case 'name':
        if (e.target.value.length < 3) {
          setNameValid(false);
        }
        break;
      case 'email':
        if (!emailRegex.test(e.target.value)) {
          setEmailValid(false);
        }
        break;
    }
  };

  return (
    <form className="order-form" onChange={() => setFormFilled(true)}>
      <label>
        Name:
        <TextInput
          type="text"
          value={name}
          onFocus={onInputFocus}
          onBlur={onInputBlur}
          onChange={(e) => setName(e.target.value)}
          placeholder="Your name..."
          data-field="name"
        />
      </label>
      {!isNameValid ? (
        <span style={{ color: 'red' }}>Name is too short!</span>
      ) : (
        ''
      )}

      <label>
        Email:
        <TextInput
          type="email"
          value={email}
          onFocus={onInputFocus}
          onBlur={onInputBlur}
          onChange={(e) => setEmail(e.target.value)}
          placeholder="Your email..."
          data-field="email"
        />
      </label>
      {!isEmailValid ? (
        <span style={{ color: 'red' }}>Incorrect email!</span>
      ) : (
        ''
      )}

      <div className="order-options-box">
        <label className="order-clock-size">
          Clock size:
          <ClocksizeRadio setClockSize={setClockSize} />
        </label>

        <label>
          City:
          <CitySelect setCityId={setCityId} />
        </label>

        <label>
          Date:
          <DatePicker setDate={setDate} checked={date}/>
        </label>

        <label>
          Time:
          <TimeSelect setTime={setTime} />
        </label>
      </div>

      {showMasters ? (
        <MastersSelect
          orderOptions={{ clockSize, cityId, date, time }}
          setMaster={setMaster}
        />
      ) : (
        ''
      )}
      {!isFormFilled ? (
        <span style={{ color: 'red' }}>Fill all fields!</span>
      ) : (
        ''
      )}
      <SubmitButton value="Make order" onClick={onOrderBtnClick} />
    </form>
  );
};

export default OrderForm;
