import React, { useEffect, useState } from 'react';
import './AdminPanel.css';
import AdminNav from '../Components/AdminPanel/ui/AdminNav';
import AdminLogin from '../Components/AdminPanel/ui/AdminLogin';
import { AdminContext } from '../context/index';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import { adminRoutes } from '../router/index';

const AdminPanel = () => {
  const { path } = useRouteMatch();
  const [isAuth, setIsAuth] = useState(false);

  useEffect(async () => {
    const storageToken = localStorage.getItem('token');
    if (storageToken) {
      const response = await fetch(process.env.API_URL + 'admin', {
        headers: {
          authorization: `Bearer ${storageToken}`,
        },
      });

      if (response.status === 200) {
        setIsAuth(true);
      }
    }
  }, []);

  return isAuth ? (
    <AdminContext.Provider value={{ isAuth, setIsAuth }}>
      <AdminNav setIsAuth={setIsAuth} />
      <Switch>
        {adminRoutes.map((route) => (
          <Route
            key={route.path}
            component={route.component}
            path={`${path}${route.path}`}
            exact={route.exact}
          ></Route>
        ))}
        <Redirect to="/admin" />
      </Switch>
    </AdminContext.Provider>
  ) : (
    <AdminLogin setIsAuth={setIsAuth} />
  );
};

export default AdminPanel;
