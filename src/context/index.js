import { createContext } from 'react';

export const SuccessfulOrderContext = createContext(null);
export const AdminContext = createContext(false);
