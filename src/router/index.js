import CitiesList from '../Components/AdminPanel/CitiesList';
import ClientsList from '../Components/AdminPanel/ClientsList';
import OrdersList from '../Components/AdminPanel/OrdersList';
import MastersList from '../Components/AdminPanel/MastersList';
import AdminPanel from '../pages/AdminPanel';
import OrderForm from '../pages/OrderForm';
import RatePage from '../pages/RatePage';
import ErrorPage from '../pages/ErrorPage';
import SuccessfulOrder from '../pages/SuccessfulOrder';

export const routes = [
  { path: '/', component: OrderForm, exact: true },
  { path: '/admin', component: AdminPanel, exact: false },
  { path: '/successfulorder/:id', component: SuccessfulOrder, exact: true },
  { path: '/ratemaster', component: RatePage, exact: true },
  { path: '/error', component: ErrorPage, exact: true },
];

export const adminRoutes = [
  { path: '/clients', component: ClientsList, exact: true },
  { path: '/orders', component: OrdersList, exact: true },
  { path: '/cities', component: CitiesList, exact: true },
  { path: '/masters', component: MastersList, exact: true },
];
