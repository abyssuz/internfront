import React from 'react';

const TimeSelect = ({ setTime, checked }) => {
  const WORKING_HOURS = [9, 10, 11, 12, 13, 14, 15, 16, 17];
  return (
    <select
      className="order-time"
      onChange={(e) => setTime(e.target.value)}
      defaultValue={checked || ''}
    >
      <option value="">Choose time</option>
      {WORKING_HOURS.map((el) => (
        <option key={el} value={el}>{`${el}:00`}</option>
      ))}
    </select>
  );
};

export default TimeSelect;
