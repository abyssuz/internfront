import React, { useEffect, useMemo } from 'react';

const DatePicker = ({ setDate, checked }) => {
  const dateRange = useMemo(() => {
    const date = new Date();

    let day = date.getDate();
    day = day < 10 ? '0' + day : String(day);

    let month = date.getMonth() + 1;
    month = month < 10 ? '0' + month : String(month);

    const year = date.getFullYear();

    return `${year}-${month}-${day}`;
  });

  return (
    <input
      className="order-date"
      value={checked || ''}
      type="date"
      min={dateRange}
      onChange={(e) => setDate(e.target.value)}
    ></input>
  );
};

export default DatePicker;
