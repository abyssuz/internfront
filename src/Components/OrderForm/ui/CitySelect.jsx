import React, { useState, useEffect } from 'react';

const CitySelect = ({ setCityId }) => {
  const [list, setList] = useState([]);

  useEffect(async () => {
    const response = await fetch(process.env.API_URL + 'cities');
    const cities = await response.json();
    setList(cities);
  }, []);

  return (
    <select className="order-city" onChange={(e) => setCityId(e.target.value)}>
      <option value="">Choose city</option>
      {list.map((city) => (
        <option key={city.id} value={city.id}>
          {city.name}
        </option>
      ))}
    </select>
  );
};

export default CitySelect;
