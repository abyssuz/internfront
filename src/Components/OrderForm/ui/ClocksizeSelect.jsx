import React, { useEffect } from 'react';
import './ClocksizeSelect.css';

const ClocksizeRadio = ({ setClockSize, checked }) => {
  useEffect(() => {
    if (checked) {
      switch (checked) {
        case 'small':
          document.querySelector(`#radio-1`).checked = true;
          break;
        case 'medium':
          document.querySelector(`#radio-2`).checked = true;
          break;
        case 'large':
          document.querySelector(`#radio-3`).checked = true;
          break;
      }
    }
  }, []);
  return (
    <div
      className="clock-size-select"
      onChange={(e) => setClockSize(e.target.value)}
    >
      <div className="clocksize-radio">
        <input id="radio-1" type="radio" name="clocksize" value="small" />
        <label htmlFor="radio-1">Small</label>
      </div>
      <div className="clocksize-radio">
        <input id="radio-2" type="radio" name="clocksize" value="medium" />
        <label htmlFor="radio-2">Medium</label>
      </div>
      <div className="clocksize-radio">
        <input id="radio-3" type="radio" name="clocksize" value="large" />
        <label htmlFor="radio-3">Large</label>
      </div>
    </div>
  );
};

export default ClocksizeRadio;
