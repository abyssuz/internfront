import React, { useEffect, useMemo, useState } from 'react';
import './MastersSelect.css';

const MastersSelect = ({ orderOptions, setMaster }) => {
  const [list, setList] = useState([]);
  const FREE_MASTERS_URL_TEMPLATE = process.env.API_URL + 'masters/free?clockSize={clockSize}&date={date}&time={time}&cityId={cityId}';

  const url = useMemo(() => {
    return Object.keys(orderOptions).reduce((acc, key) => {
      return acc.replace(`{${key}}`, orderOptions[key]);
    }, FREE_MASTERS_URL_TEMPLATE);
  }, [orderOptions]);

  useEffect(async () => {
    const response = await fetch(url);
    const masters = await response.json();
    setList(masters);
    clearMaster();
    return () => clearMaster();
  }, [url]);

  const clearMaster = () => {
    setMaster({ id: '', name: '' });
    document.querySelector('.order-master.active')?.classList.toggle('active');
  };

  const onMasterClick = (target, id, name) => {
    document.querySelector('.order-master.active')?.classList.toggle('active');
    target.classList.toggle('active');
    setMaster({ id, name });
  };

  return (
    <div className="masters-select">
      {list.length !== 0 ? (
        list.map((el) => {
          return (
            <p
              key={el.id}
              className="order-master"
              onClick={(e) => onMasterClick(e.target, el.id, el.name)}
            >{`Name: ${el.name}, rating: ${
              el.rating.length !== 0
                ? el.rating.reduce((acc, curr) => acc + curr) / el.rating.length
                : 0
            }`}</p>
          );
        })
      ) : (
        <p className="nomasters-msg">
          No free masters related to options. Please, try to change options
        </p>
      )}
    </div>
  );
};

export default MastersSelect;
