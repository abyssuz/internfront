import React from 'react';
import './TextInput.css';

const TextInput = (props) => {
  return <input className="text-input" {...props}></input>;
};

export default TextInput;
