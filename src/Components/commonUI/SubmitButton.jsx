import React from 'react';
import './SubmitButton.css';

const SubmitButton = (props) => {
  return <input className="submit-btn" type="submit" {...props} />;
};

export default SubmitButton;
