import React, { useState } from 'react';
import SubmitButton from '../commonUI/SubmitButton';
import AdminModal from './ui/AdminModal';
import ClocksizeSelect from '../OrderForm/ui/ClocksizeSelect';
import { useManageList } from '../../hooks/useManageList';
import DatePicker from '../OrderForm/ui/DatePicker';
import TimeSelect from '../OrderForm/ui/TimeSelect';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import './tables.css';

const OrdersList = () => {
  const [editModal, setEditModal] = useState(false);
  const [fields, setFields] = useState({});

  const [list, remove, add, edit] = useManageList(
    process.env.API_URL + 'orders'
  );

  const onListItemClick = (el) => {
    setFields(el);
    console.log(el);
    setEditModal(true);
  };

  const onSaveEditBtnClick = () => {
    edit({
      id: fields.id,
      name: fields.client.name,
      email: fields.client.email,
      clockSize: fields.clockSize,
      city: fields.city,
      date: fields.date,
      time: fields.startTime,
      masterId: fields.masterId,
    });
    clearFields();
    setEditModal(false);
  };

  const onDeleteBtnClick = () => {
    remove(fields.id);
    clearFields();
    setEditModal(false);
  };

  const clearFields = () => {
    const clearedFields = {};
    Object.keys(fields).forEach((el) => {
      clearedFields[el] = '';
    });
    setFields(clearedFields);
  };

  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell width='50px'><label>№</label></TableCell>
            <TableCell><label>Client Email</label></TableCell>
            <TableCell><label>Master</label></TableCell>
            <TableCell><label>Clock size</label></TableCell>
            <TableCell><label>City</label></TableCell>
            <TableCell><label>Date</label></TableCell>
            <TableCell><label>Time</label></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.map((el, index) => (
            <TableRow onClick={() => onListItemClick(el)} hover={true}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{el.client.email}</TableCell>
              <TableCell>{el.master.name}</TableCell>
              <TableCell>{el.clockSize}</TableCell>
              <TableCell>{el.city}</TableCell>
              <TableCell>{el.date}</TableCell>
              <TableCell>{el.startTime}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      {editModal ? (
        <AdminModal setVisible={setEditModal} clearFields={clearFields}>
          <div className="order-options-box">
            <label>Clock size</label>
            <ClocksizeSelect
              setClockSize={(value) =>
                setFields({ ...fields, clockSize: value })
              }
              checked={fields.clockSize}
            />
            <label>Date</label>
            <DatePicker
              setDate={(value) => setFields({ ...fields, date: value })}
              checked={fields.date}
            />
            <label>Time</label>
            <TimeSelect
              setTime={(value) => setFields({ ...fields, startTime: value })}
              checked={fields.startTime}
            />
          </div>
          <div className="modal-form-btns">
            <SubmitButton value="Save" onClick={onSaveEditBtnClick} />
            <SubmitButton value="Delete" onClick={onDeleteBtnClick} />
          </div>
        </AdminModal>
      ) : (
        ''
      )}
    </div>
  );
};

export default OrdersList;
