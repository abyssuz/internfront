import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';

const AdminNav = ({ setIsAuth }) => {
  const { path, url } = useRouteMatch();

  const onLogout = () => {
    localStorage.removeItem('token');
    setIsAuth(false);
  };

  return (
    <nav className="admin-nav">
      <Link className="admin-nav-links" to={`${url}/clients`}>
        Clients
      </Link>
      <Link className="admin-nav-links" to={`${url}/cities`}>
        Cities
      </Link>
      <Link className="admin-nav-links" to={`${url}/orders`}>
        Orders
      </Link>
      <Link className="admin-nav-links" to={`${url}/masters`}>
        Masters
      </Link>
      <Link className="admin-nav-links" to={`${url}`} onClick={onLogout}>
        Logout
      </Link>
    </nav>
  );
};

export default AdminNav;
