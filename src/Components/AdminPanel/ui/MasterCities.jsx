import React from 'react';
import { useManageList } from '../../../hooks/useManageList';
import './MasterCities.css';

const MasterCities = ({ citiesId, setCitiesId }) => {
  const [cities] = useManageList(process.env.API_URL + 'cities');

  const onCitySelect = (e) => {
    if (citiesId.includes(+e.target.value))
      setCitiesId(citiesId.filter((el) => el !== +e.target.value));
    else setCitiesId([...citiesId, +e.target.value]);
  };

  return (
    <div className="master-cities-select">
      {cities.map((el) => (
        <div key={el.id} className="master-cities-checkbox">
          <input
            id={el.id}
            type="checkbox"
            name="cities"
            value={el.id}
            checked={citiesId?.includes(el.id)}
            onChange={onCitySelect}
          />
          <label htmlFor={el.id}>{el.name}</label>
        </div>
      ))}
    </div>
  );
};

export default MasterCities;
