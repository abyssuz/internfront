import React from 'react';
import TextInput from '../../commonUI/TextInput';
import './ModalForm.css';

const ModalForm = ({ children, fields, setFields }) => {
  return (
    <div className="modal-form">
      {Object.keys(fields).map((el, index) =>
        el.match(/id|link|client|master|endtime|city/gi) === null ? (
          <label key={index}>
            {el.toUpperCase()}
            <TextInput
              type="text"
              placeholder={el}
              value={fields[el]}
              onChange={(e) => setFields({ ...fields, [el]: e.target.value })}
            ></TextInput>
          </label>
        ) : (
          ''
        )
      )}
      {children}
    </div>
  );
};

export default ModalForm;
