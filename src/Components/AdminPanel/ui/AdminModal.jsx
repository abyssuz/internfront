import React from 'react';
import './AdminModal.css';

const AdminModal = ({ children, setVisible, clearFields }) => {
  const onBackdropClick = () => {
    clearFields();
    setVisible(false);
  };

  return (
    <div className="admin-modal" onClick={onBackdropClick}>
      <div className="admin-modal-content" onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

export default AdminModal;
