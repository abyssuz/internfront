import React, { useState } from 'react';
import TextInput from '../../commonUI/TextInput';
import SubmitButton from '../../commonUI/SubmitButton';
import './AdminLogin.css';

const LoginForm = ({ setIsAuth }) => {
  const [adminEmail, setAdminEmail] = useState('');
  const [adminPassword, setAdminPassword] = useState('');
  const [isWrong, setisWrong] = useState(false);

  const onLoginBtnClick = async (e) => {
    e.preventDefault();

    const response = await fetch(process.env.API_URL + 'admin', {
      method: 'POST',
      body: JSON.stringify({ email: adminEmail, password: adminPassword }),
      headers: { 'Content-Type': 'application/json' },
    });
    const { token } = await response.json();

    if (response.status === 200) {
      localStorage.setItem('token', token);
      setIsAuth(true);
    } else {
      setisWrong(true);
    }
  };

  return (
    <form className="admin-login">
      <TextInput
        type="email"
        placeholder={'Email...'}
        value={adminEmail}
        onChange={(e) => setAdminEmail(e.target.value)}
      ></TextInput>
      <TextInput
        type="password"
        placeholder={'Password...'}
        value={adminPassword}
        onChange={(e) => setAdminPassword(e.target.value)}
      ></TextInput>
      {isWrong ? (
        <p style={{ color: 'red', marginBottom: 10 }}>
          Wrong email or password
        </p>
      ) : (
        ''
      )}
      <SubmitButton value="Login" onClick={onLoginBtnClick} />
    </form>
  );
};

export default LoginForm;
