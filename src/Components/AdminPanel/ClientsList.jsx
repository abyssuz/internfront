import React, { useState } from 'react';
import SubmitButton from '../commonUI/SubmitButton';
import AdminModal from './ui/AdminModal';
import ModalForm from './ui/ModalForm';
import { useManageList } from '../../hooks/useManageList';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import './tables.css';

const ClientsList = () => {
  const [editModal, setEditModal] = useState(false);
  const [fields, setFields] = useState({});

  const [list, remove, _, edit] = useManageList(
    process.env.API_URL + 'clients'
  );

  const onListItemClick = (el) => {
    setFields(el);
    setEditModal(true);
  };

  const onSaveEditBtnClick = () => {
    edit(fields);
    clearFields();
    setEditModal(false);
  };

  const onDeleteBtnClick = () => {
    remove(fields.id);
    clearFields();
    setEditModal(false);
  };

  const clearFields = () => {
    const clearedFields = {};
    Object.keys(fields).forEach((el) => {
      clearedFields[el] = '';
    });
    setFields(clearedFields);
  };

  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell width='50px'><label>№</label></TableCell>
            <TableCell><label>Name</label></TableCell>
            <TableCell><label>Email</label></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.map((el, index) => (
            <TableRow onClick={() => onListItemClick(el)} hover={true}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{el.name}</TableCell>
              <TableCell>{el.email}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      {editModal ? (
        <AdminModal setVisible={setEditModal} clearFields={clearFields}>
          <ModalForm fields={fields} setFields={setFields}>
            <div className="modal-form-btns">
              <SubmitButton value="Save" onClick={onSaveEditBtnClick} />
              <SubmitButton value="Delete" onClick={onDeleteBtnClick} />
            </div>
          </ModalForm>
        </AdminModal>
      ) : (
        ''
      )}
    </div>
  );
};

export default ClientsList;
