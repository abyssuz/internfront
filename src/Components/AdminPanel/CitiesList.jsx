import React, { useState } from 'react';
import SubmitButton from '../commonUI/SubmitButton';
import AdminModal from './ui/AdminModal';
import ModalForm from './ui/ModalForm';
import { useManageList } from '../../hooks/useManageList';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import './tables.css';

const CitiesList = () => {
  const [addModal, setAddModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [fields, setFields] = useState({ id: '', name: '' });

  const [list, remove, add, edit] = useManageList(process.env.API_URL + 'cities');

  const onListItemClick = (el) => {
    setFields(el);
    setEditModal(true);
  };

  const onAddBtnClick = () => {
    add(fields);
    clearFields();
    setAddModal(false);
  };

  const onSaveEditBtnClick = () => {
    edit(fields);
    clearFields();
    setEditModal(false);
  };

  const onDeleteBtnClick = () => {
    remove(fields.id);
    clearFields();
    setEditModal(false);
  };

  const clearFields = () => {
    const clearedFields = {};
    Object.keys(fields).forEach((el) => {
      clearedFields[el] = '';
    });
    setFields(clearedFields);
  };

  return (
    <div>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell width='50px'><label>№</label></TableCell>
            <TableCell><label>Name</label></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list.map((el, index) => (
            <TableRow onClick={() => onListItemClick(el)} hover={true}>
              <TableCell>{index + 1}</TableCell>
              <TableCell>{el.name}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>

      {addModal ? (
        <AdminModal setVisible={setAddModal} clearFields={clearFields}>
          <ModalForm fields={fields} setFields={setFields}>
            <SubmitButton value="Add" onClick={onAddBtnClick} />
          </ModalForm>
        </AdminModal>
      ) : (
        ''
      )}

      {editModal ? (
        <AdminModal setVisible={setEditModal} clearFields={clearFields}>
          <ModalForm fields={fields} setFields={setFields}>
            <div className="modal-form-btns">
              <SubmitButton value="Save" onClick={onSaveEditBtnClick} />
              <SubmitButton value="Delete" onClick={onDeleteBtnClick} />
            </div>
          </ModalForm>
        </AdminModal>
      ) : (
        ''
      )}

      <SubmitButton value="Add city" onClick={() => setAddModal(true)} />
    </div>
  );
};

export default CitiesList;
